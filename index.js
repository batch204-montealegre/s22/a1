// alert("Hello");

/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];
 let lowercase=[];
let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - This function should be able to receive a string.
        - Determine if the input username already exists in our registeredUsers array.
            - If it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            - If it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - Invoke and register a new user in the browser console.
        - In the browser console, log the registeredUsers array.


*/

function need2Register(username){

	if (registeredUsers.includes(username) ||lowercase.includes(username)){

		alert("Registration failed. Username " + username + " already exists!");
		


	}

		else{

             registeredUsers.push(username);
             

             registeredUsers.forEach(function(username){
                lowercase.push(username.toLowerCase());
             });
              
            

			alert("Thank you for registering! " +username);

            console.log(" Registered Users: ");
              console.log(registeredUsers);
            
          	}

}

need2Register("Roxanne Montealegre");

  







/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - This function should be able to receive a string.
        - Determine if the input username exists in our registeredUsers array.
            - If it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - If it is not, show an alert window with the following message:
                - "User not found."
        - Invoke the function and add a registered user in your friendsList in the browser console.
        - In the browser console, log the friendsList array in the console.

*/

function addFriend(friend){

    

    if (registeredUsers.includes(friend) || lowercase
        .includes(friend))
    {
        console.log ("\nFriend");

        friendsList.push(friend);
        alert('You have added ' +friend +' as a friend!');
        console.log(friendsList);

        console.log('Number of friends: ' +friendsList.length);
        
        console.log('\n Friends List :');
        friendsList.forEach(function(showFriends){
        console.log(showFriends);

        });
        
    }

    else{

        alert("User " +friend + " not found. Please register");

    }
}



addFriend('Roxanne Montealegre');




/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.

*/


function displayFriends(){
    if (friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    }
    else{
        friendsList.forEach(function(showFriends){
            // console.log(showFriends);
        });
    }
}
displayFriends();


/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function in the browser console.

*/
function numOfFriends(){
    console.log('Number of friends: ' +friendsList.length);
    if (friendsList.length === 0){
        alert("You currently have 0 friends. Add one first.");
    }

    else{
        alert( "You currently have " +friendsList.length + " friends.");
    }

}

numOfFriends();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function in the browser console.
        - In the browser console, log the friendsList array.

*/ function deleteFriend() {
        let friends = friendsList.length;
        if(friends) {
            let deleted = friendsList.pop();
            alert('Successfully deleted ' +deleted + ' on your Friends list.');
            console.log('\nNumber of friends: ' +friendsList.length);
            
            console.log('\n Friends List :');
            friendsList.forEach(function(showFriends){
            console.log(showFriends);
            });


        } else {
            alert('You currently have 0 friends. Nothing to delete');
        }
    }

    deleteFriend();

    


